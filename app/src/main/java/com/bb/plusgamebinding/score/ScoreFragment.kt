package com.bb.plusgamebinding.score

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.bb.plusgamebinding.R
import com.bb.plusgamebinding.databinding.FragmentScoreBinding
import com.bb.plusgamebinding.game.PlusGameFragmentArgs

class ScoreFragment : Fragment() {
    private lateinit var viewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentScoreBinding>(inflater, R.layout.fragment_score, container, false)
        viewModelFactory = ScoreViewModelFactory(
            PlusGameFragmentArgs.fromBundle(requireArguments()).correct,
            PlusGameFragmentArgs.fromBundle(requireArguments()).incorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(ScoreViewModel::class.java)


        binding.btnPlayAgain.setOnClickListener { view ->
            view.findNavController().navigate(
                ScoreFragmentDirections.actionScoreFragmentToTitleFragment(0,0))
        }

        viewModel.correct.observe(this, Observer { newCorrect ->
            binding.txtCorrect.text = getString(R.string.scoreCorrect, newCorrect)
        })
        viewModel.incorrect.observe(this, Observer { newIncorrect ->
            binding.txtIncorrect.text = getString(R.string.scoreIncorrect, newIncorrect)
        })

        binding.lifecycleOwner = this

        return binding.root
    }
}