package com.bb.plusgamebinding.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

//@Database(entities = [MathGame::class], version = 1, exportSchema = false)
//abstract class MathGameDatabase : RoomDatabase(){
//    abstract val mathGameDatabaseDAO: MathGameDatabaseDAO
//    companion object {
//        @Volatile
//        private var INSTANCE: MathGameDatabase? = null
//
//        fun getInstance(context: Context): MathGameDatabase {
//            synchronized(this) {
//                var instance = INSTANCE
//                if (instance == null) {
//                    instance = Room.databaseBuilder(context.applicationContext, MathGameDatabase::class.java, "mathGame_history_database")
//                        .fallbackToDestructiveMigration()
//                        .build()
//                    INSTANCE = instance
//                }
//                return instance
//            }
//        }
//    }
//}