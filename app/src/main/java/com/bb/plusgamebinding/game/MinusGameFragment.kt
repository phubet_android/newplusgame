package com.bb.plusgamebinding.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.bb.plusgamebinding.game.MinusGameFragmentArgs
import com.bb.plusgamebinding.game.MinusGameFragmentDirections
import com.bb.plusgamebinding.R
import com.bb.plusgamebinding.databinding.FragmentMinusgameBinding

class MinusGameFragment : Fragment() {
    // Binging
    private lateinit var binding: FragmentMinusgameBinding

    // ViewModel
    private lateinit var viewModel: MinusGameViewModel

    // ViewModelFactory
    private lateinit var viewModelFactory: MinusGameViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Binding
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_minusgame, container, false)

        // ViewModelFactory
        viewModelFactory = MinusGameViewModelFactory(
            MinusGameFragmentArgs.fromBundle(requireArguments()).correct,
            MinusGameFragmentArgs.fromBundle(requireArguments()).incorrect
        )

        // ViewModel
        viewModel = ViewModelProvider(this, viewModelFactory).get(MinusGameViewModel::class.java)

        // onClickEndGame
        viewModel.eventEndGame.observe(viewLifecycleOwner, Observer { EndGame ->
            if (EndGame) {
                val action = MinusGameFragmentDirections.actionMinusGameFragmentToScoreFragment(
                    viewModel.correct.value ?: 0,
                    viewModel.incorrect.value ?: 0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })

        // onClickMenu
        viewModel.eventMenu.observe(viewLifecycleOwner, { Menu ->
            if (Menu) {
                val action = MinusGameFragmentDirections.actionMinusGameFragmentToTitleFragment(
                    viewModel.correct.value ?: 0,
                    viewModel.incorrect.value ?: 0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })

        binding.minusGameViewModel = viewModel
        binding.lifecycleOwner = this

        //Play Game
        viewModel.generateQuestion(binding)

        return binding.root
    }
}