package com.bb.plusgamebinding.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MultiplyGameViewModelFactory(private val correctLast: Int, private val incorrectLast: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MultiplyGameViewModel::class.java)) {
            return MultiplyGameViewModel(correctLast, incorrectLast) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}