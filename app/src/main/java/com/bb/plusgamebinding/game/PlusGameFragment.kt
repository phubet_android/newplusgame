package com.bb.plusgamebinding.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.bb.plusgamebinding.R
import com.bb.plusgamebinding.databinding.FragmentPlusgameBinding

class PlusGameFragment : Fragment() {
    // Binging
    private lateinit var binding: FragmentPlusgameBinding
    // ViewModel
    private lateinit var viewModel: PlusGameViewModel
    // ViewModelFactory
    private lateinit var viewModelFactory: PlusGameViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Binding
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_plusgame, container, false)

        // ViewModelFactory
        viewModelFactory = PlusGameViewModelFactory(
            PlusGameFragmentArgs.fromBundle(requireArguments()).correct,
            PlusGameFragmentArgs.fromBundle(requireArguments()).incorrect
        )

        // ViewModel
        viewModel = ViewModelProvider(this, viewModelFactory).get(PlusGameViewModel::class.java)

        // onClickEndGame
        viewModel.eventEndGame.observe(viewLifecycleOwner, Observer { EndGame ->
            if(EndGame) {
                val action = PlusGameFragmentDirections.actionPlusGameFragmentToScoreFragment(
                    viewModel.correct.value?:0,
                    viewModel.incorrect.value?:0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })

        // onClickMenu
        viewModel.eventMenu.observe(viewLifecycleOwner, { Menu ->
            if(Menu) {
                val action = PlusGameFragmentDirections.actionPlusGameFragmentToTitleFragment(
                    viewModel.correct.value?:0,
                    viewModel.incorrect.value?:0
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })

        //ViewModel data binding
        binding.plusGameViewModel = viewModel

        binding.lifecycleOwner = this

        //Play Game
        viewModel.generateQuestion(binding)

        return binding.root
    }
}
