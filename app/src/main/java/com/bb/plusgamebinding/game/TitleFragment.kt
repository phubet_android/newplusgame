package com.bb.plusgamebinding.game

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.bb.plusgamebinding.R
import com.bb.plusgamebinding.databinding.FragmentTitleBinding


class titleFragment : Fragment() {
    // ViewModel
//    private lateinit var viewModel: PlusGameViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater,
            R.layout.fragment_title, container, false)
        val args = titleFragmentArgs.fromBundle(arguments!!)
        // ViewModel
//        viewModel = ViewModelProvider(this).get(PlusGameViewModel::class.java)

        binding.playPlusGameButton.setOnClickListener { view ->
          view.findNavController().navigate(
              titleFragmentDirections.actionTitleFragmentToPlusGameFragment(
                  args.correct,
                  args.incorrect
              )
          )
        }
        binding.playMultiGameButton.setOnClickListener { view ->
            view.findNavController().navigate(
                titleFragmentDirections.actionTitleFragmentToMultiplyGameFragment(
                    args.correct,
                    args.incorrect
                )
            )
        }
        binding.playMinusGameButton.setOnClickListener { view ->
            view.findNavController().navigate(
                titleFragmentDirections.actionTitleFragmentToMinusGameFragment(
                    args.correct,
                    args.incorrect
                )
            )
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!, view!!.findNavController()) || super.onOptionsItemSelected(item)
    }
}