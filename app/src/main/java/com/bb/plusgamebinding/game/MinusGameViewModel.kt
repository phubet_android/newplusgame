package com.bb.plusgamebinding.game

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bb.plusgamebinding.databinding.FragmentMinusgameBinding

class MinusGameViewModel(correctLast: Int, incorrectLast: Int) : ViewModel() {
    private val _answer = MutableLiveData<Int>()
    val answer: LiveData<Int>
        get() = _answer

    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    private val _eventMenu = MutableLiveData<Boolean>()
    val eventMenu: LiveData<Boolean>
        get() = _eventMenu

    private val _eventEndGame = MutableLiveData<Boolean>()
    val eventEndGame: LiveData<Boolean>
        get() = _eventEndGame

    init {
        Log.i("GameViewModel", "GameViewModel created!!!")
        _answer.value = 0
        _correct.value = correctLast
        _incorrect.value = incorrectLast
    }

    fun onCorrect() {
        _correct.value = _correct.value?.plus(1)
    }

    fun onIncorrect() {
        _incorrect.value = _incorrect.value?.plus(1)
    }

    fun onMenu() {
        _eventMenu.value = true
    }

    fun onEndGame() {
        _eventEndGame.value = true
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
    }

    fun generateQuestion(binding: FragmentMinusgameBinding) {
        binding.apply {
            val randomNum1 = (0..10).random()
            val randomNum2 = (0..10).random()
            num1.text = randomNum1.toString()
            num2.text = randomNum2.toString()
            val ans = randomNum1 - randomNum2
            _answer.value = ans
            generateAnswer(binding)
        }
    }

    private fun generateAnswer(binding: FragmentMinusgameBinding) {
        binding.apply {
            val answers = arrayOf(
                "btnAns1", "btnAns2", "btnAns3"
            )
            val btnRandom = answers[(0..2).random()]
            if (btnRandom == "btnAns1") {
                btnAns1.text = answer.value.toString()
                btnAns2.text = (-10..10).random().toString()
                btnAns3.text = (-10..10).random().toString()
            } else if (btnRandom == "btnAns2") {
                btnAns1.text = (-10..10).random().toString()
                btnAns2.text = answer.value.toString()
                btnAns3.text = (-10..10).random().toString()
            } else {
                btnAns1.text = (-10..10).random().toString()
                btnAns2.text = (-10..10).random().toString()
                btnAns3.text = answer.value.toString()
            }
            checkAnswer(correct.value ?: 0, incorrect.value ?: 0, binding)
        }
    }

    private fun checkAnswer(correct: Int, incorrect: Int, binding: FragmentMinusgameBinding) {
        var correctScore = correct
        var incorrectScore = incorrect
        binding.apply {
            // CheckAnswer
            btnAns1.setOnClickListener {
                val checkAns = btnAns1.text.toString()
                if (checkAns.toInt() == answer.value) {
                    correctScore++
                    onCorrect()
                } else {
                    incorrectScore++
                    onIncorrect()
                }
                generateQuestion(binding)
            }

            btnAns2.setOnClickListener {
                val checkAns = btnAns2.text.toString()
                if (checkAns.toInt() == answer.value) {
                    correctScore++
                    onCorrect()
                } else {
                    incorrectScore++
                    onIncorrect()
                }
                generateQuestion(binding)
            }

            btnAns3.setOnClickListener {
                val checkAns = btnAns3.text.toString()
                if (checkAns.toInt() == answer.value) {
                    correctScore++
                    onCorrect()
                } else {
                    incorrectScore++
                    onIncorrect()
                }
                generateQuestion(binding)
            }
        }
    }
}