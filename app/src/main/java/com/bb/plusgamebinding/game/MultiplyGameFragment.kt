package com.bb.plusgamebinding.game

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.bb.plusgamebinding.R
import com.bb.plusgamebinding.databinding.FragmentMultiplygameBinding

class MultiplyGameFragment : Fragment() {
    // Binging
    private lateinit var binding: FragmentMultiplygameBinding

    // ViewModel
    private lateinit var viewModel: MultiplyGameViewModel

    // ViewModelFactory
    private lateinit var viewModelFactory: MultiplyGameViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Binding
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_multiplygame, container, false)

        // ViewModelFactory
        viewModelFactory = MultiplyGameViewModelFactory(
            MultiplyGameFragmentArgs.fromBundle(requireArguments()).correct,
            MultiplyGameFragmentArgs.fromBundle(requireArguments()).incorrect
        )

        // ViewModel
        viewModel = ViewModelProvider(this, viewModelFactory).get(MultiplyGameViewModel::class.java)

        // onClickEndGame
        viewModel.eventEndGame.observe(viewLifecycleOwner, Observer { EndGame ->
            if (EndGame) {
                val action =
                    MultiplyGameFragmentDirections.actionMultiplyGameFragmentToScoreFragment(
                        viewModel.correct.value ?: 0,
                        viewModel.incorrect.value ?: 0
                    )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })

        // onClickMenu
        viewModel.eventMenu.observe(viewLifecycleOwner, { Menu ->
            if (Menu) {
                val action =
                    MultiplyGameFragmentDirections.actionMultiplyGameFragmentToTitleFragment(
                        viewModel.correct.value ?: 0,
                        viewModel.incorrect.value ?: 0
                    )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })

        binding.multiplyGameViewModel = viewModel
        binding.lifecycleOwner = this

        //Play Game
        viewModel.generateQuestion(binding)

        return binding.root
    }
}
