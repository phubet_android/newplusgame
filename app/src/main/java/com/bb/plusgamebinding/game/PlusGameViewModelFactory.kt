package com.bb.plusgamebinding.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PlusGameViewModelFactory (private val correctLast: Int, private val incorrectLast: Int): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlusGameViewModel::class.java)) {
            return PlusGameViewModel(correctLast, incorrectLast) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}